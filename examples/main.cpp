
// #include <iostream>

// #include <notk/controller.hpp>
// #include <common_tools/boostlog.hpp>

// double fit(const double& x)
// {
//     return x * x;
// }
// double fit1(const std::vector<double>& x)
// {
//     return x[0] * x[0] + x[1] * x[1];
// }
// double Rosenbrock(const std::vector<double>& population)
// {
//     double f = 0;
//     for (size_t i = 0; i < population.size() - 1; i++)
//     {
//         f = f + (100 * (population[i + 1] - population[i] * population[i]) *
//                      (population[i + 1] - population[i] * population[i]) +
//                  (population[i] - 1) * (population[i] - 1));
//     }
//     return f;
// }

// int main()
// {
//     std::vector<std::pair<std::string, std::string>> tt = {{"NOTK_RES", "NOTK_RES"}};
//     initboostlog::init(sev_lvl::trace, "notk_test", tt);

//     notk::OptController<double, double> optController;
//     const std::string error_msg = "An error occurred. For datails please see the log files.";

//     if (!optController.add_problem_config(
//             "/data/Projects/www/altsybeyev_server/num_meth/data/opimization/ball.json"))
//     {
//         std::cout << error_msg << std::endl;
//     }

//     if (!optController.set_fitness(Rosenbrock))
//     {
//         std::cout << error_msg << std::endl;
//     }
//     bool                     flag_abort = true;
//     std::vector<std::string> status;
//     auto                     results = optController.process(flag_abort, status);

//     /* notk::OptStepsContainer<double, double> container(
//          "/data/Projects/optimization_lib/optimization-library/example/example.json");

//      notk::UniformSearch1d<double, double> uniformSearch1d;
//      uniformSearch1d.set_fitness(fit);
//      uniformSearch1d.opt_routine();*/
//     return 0;
// }