
#include <boost/algorithm/string.hpp>
#include <boost/program_options.hpp>

#include <iostream>

#include <controller.h>

#include "../testfunctions/testfunctions.h"

namespace po = boost::program_options;

notk::OptController<double, double> optController;
std::string last_path;

const std::string error_msg = "An error occurred. For datails please see the log files.";

bool config(const std::string& path)
{
    last_path = path;

    return optController.add_problem_config(path);
}
bool config()
{
    return optController.add_problem_config(last_path);
}

int main(int argc, char** argv)
{
    bool                     flag_abort = false;
    std::vector<std::string> status;
    po::options_description  desc("General options");
    std::string              task_type;
    desc.add_options()("help,h", "Show help")("quit,q", "Quit")(
        "config,c", po::value<std::string>(&task_type),
        "Set json file with optimization solvers description")(
        "reconfig,r", "Reset last json file with optimization solvers description")(
        "fitness,f", po::value<std::string>(&task_type),
        "Set fitness function. The arg defines function. (ros - Rosenbrock, ras - Rastrigin)")(
        "start,s", "Start optimization");

    std::cout << "Welcome to optimization console application! Type --help for receiving "
                 "information about commands"
              << std::endl;

    config("/data/Projects/optimization_lib/build/release/app/example.json");

    while (1)
    {
        std::string line;
        std::getline(std::cin, line);

        std::vector<std::string> strs;
        boost::split(strs, line, boost::is_any_of(" "));

        bool result = true;
        if (strs[0] == "--help" || strs[0] == "-h")
        {
            std::cout << "Application commands" << std::endl << desc << std::endl;
        }
        else if (strs[0] == "--config" || strs[0] == "-c")
        {
            if (strs.size() < 2)
            {
                std::cout << "Command argument is not defined" << std::endl;
            }
            else
            {
                result = config(strs[1]);
            }
        }
        else if (strs[0] == "--reconfig" || strs[0] == "-r")
        {
            if (last_path.size() == 0)
            {
                std::cout << "This a first call of config. Please use --config command"
                          << std::endl;
            }
            else
            {
                result = config();
            }
        }
        else if (strs[0] == "--fitness" || strs[0] == "-f")
        {
            if (strs.size() < 2)
            {
                std::cout << "Command argument is not defined" << std::endl;
            }
            else
            {
                if (strs[1] == "ros")
                {
                    result = optController.set_fitness(Rosenbrock);
                }
                else if (strs[1] == "ras")
                {
                    result = optController.set_fitness(Rastrigin);
                }
                else
                {
                    std::cout << "Unknown command" << std::endl;
                }
            }
        }
        else if (strs[0] == "--start" || strs[0] == "-s")
        {
            optController.process(flag_abort, status);
        }
        else if (strs[0] == "--quit" || strs[0] == "-q")
        {
            break;
        }
        else
        {
            std::cout << "Unknown command" << std::endl;
        }
        if (!result)
        {
            std::cout << "An error occurred. For datails please see the log files." << std::endl;
        }
    }
    return 0;
}