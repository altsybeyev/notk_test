
#include <fstream>
#include <future>
#include <iostream>

#include <common_tools/boostlog.hpp>
#include <notk/controller.hpp>
#include <notk/testfunctions.hpp>

int main(int argc, char** argv)
{
    LOG_CH(commtools::sev_lvl::info, "SYSTEM") << "Call notk web wrapper";

    if (argc != 6)
    {
        LOG_CH(commtools::sev_lvl::error, "SYSTEM")
            << "Invalid number of argument. argc = " << argc << ". Should be = " << 6;
        return 0;
    }
    std::string              files_posfix = argv[1];
    const commtools::sev_lvl sev_lvl_par  = static_cast<commtools::sev_lvl>(atoi(argv[2]));
    const int                fit_type     = atoi(argv[3]);
    const int                seconds      = atoi(argv[4]);
    const std::string        config       = argv[5];

    const std::string error_msg = "For datails please see the log files";

    std::vector<std::pair<std::string, std::string>> tt = {{"NOTK_RES", "res_" + files_posfix},
                                                           {"SYSTEM", "system_" + files_posfix}};

    commtools::BoostLog::get(sev_lvl_par, "notk" + files_posfix, "/var/log/notk_web/", tt);

    notk::NOTKController<double, double> optController;

    if (!optController.set_problem_config(config))
    {
        LOG_CH(commtools::sev_lvl::error, "SYSTEM") << "Invalid config. " << error_msg;
        return 0;
    }

    auto fitness = notk::Rastrigin;

    switch (fit_type)
    {
    case 0:
        fitness = notk::Rastrigin;
        break;
    case 1:
        fitness = notk::Rosenbrock;
        break;
    case 2:
        fitness = notk::Sphere;
        break;
    case 3:
        fitness = notk::Ackley;
        break;
    default:
        LOG_CH(commtools::sev_lvl::error, "SYSTEM") << "Unexpected fitness function type";
        return 0;
    }

    if (!optController.set_fitness(fitness))
    {
        LOG_CH(commtools::sev_lvl::error, "SYSTEM") << "Error fitness setting. " << error_msg;
        return 0;
    }

    bool flag_abort = true;
    bool flag_done  = false;

    std::vector<std::string> status;

    LOG_CH(commtools::sev_lvl::info, "SYSTEM") << "Start calculations";

    auto future = std::async([&]() {
        optController.process(flag_abort);
        LOG_CH(commtools::sev_lvl::info, "SYSTEM") << "Calculations is done";
        flag_done = true;
    });

    auto start = std::chrono::system_clock::now();

    while (!flag_done)
    {
        int elapsed_seconds = std::chrono::duration_cast<std::chrono::seconds>(
                                  std::chrono::system_clock::now() - start)
                                  .count();

        if (elapsed_seconds > seconds)
        {
            LOG_CH(commtools::sev_lvl::warning, "SYSTEM") << "Calculations time expired";

            flag_abort = false;
            flag_done  = true;
        }
    }

    future.get();

    LOG_CH(commtools::sev_lvl::info, "SYSTEM")
        << "Time elapsed, ms = "
        << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() -
                                                                 start)
               .count();

    return 0;
}